package ep2;

import java.io.*;
import java.util.logging.Level;
import java.util.logging.Logger;

public class Arquivo {
    public static int carretaDisp = 0, carroDisp = 0, motoDisp = 0, vanDisp = 0;
    public static int carretaIndisp = 0, carroIndisp = 0, motoIndisp = 0, vanIndisp = 0;
    public static double margemLucro = 0;
    public static String menorCustoNome = "Nome", maisRapidoNome = "Nome", melhorCxBNome = "Nome";
    public static double menorCustoTempo = 0, menorCustoCusto = 0, maisRapidoTempo = 0, maisRapidoCusto = 0, melhorCxBTempo = 0, melhorCxBCusto = 0; 
    public static double menorCustoLucro = 0, maisRapidoLucro = 0, melhorCxBLucro = 0;
     
    public void GetArquivoVeiculos() throws IOException{
     
        FileInputStream input = new FileInputStream("src/txt/Veiculo.txt");
        InputStreamReader entradaFormatada = new InputStreamReader(input);
        BufferedReader entradaString = new BufferedReader(entradaFormatada);
     
        String linha = new String();
     
        while(linha != null){
            linha = entradaString.readLine();
                if("Disponivel".equals(linha)){
                    linha = entradaString.readLine();
                    carretaDisp = Integer.parseInt(linha);
                    linha = entradaString.readLine();
                    carroDisp = Integer.parseInt(linha);
                    linha = entradaString.readLine();
                    motoDisp = Integer.parseInt(linha);
                    linha = entradaString.readLine();
                    vanDisp = Integer.parseInt(linha);
                }
                if("Indisponivel".equals(linha)){
                    linha = entradaString.readLine();
                    carretaIndisp = Integer.parseInt(linha);
                    linha = entradaString.readLine();
                    carroIndisp = Integer.parseInt(linha);
                    linha = entradaString.readLine();
                    motoIndisp = Integer.parseInt(linha);
                    linha = entradaString.readLine();
                    vanIndisp = Integer.parseInt(linha);
                }
        }
        input.close();
    }
    public void SetArquivoVeiculos() throws IOException{
        FileWriter output = new FileWriter("src/txt/Veiculo.txt");
        PrintWriter gravaSaida = new PrintWriter(output);
        
        if(carretaDisp < 0){
            carretaDisp = 0;
        }
        if(carroDisp < 0){
            carroDisp = 0;
        }
        if(motoDisp < 0){
            motoDisp = 0;
        }
        if(vanDisp < 0){
            vanDisp = 0;
        }
        
        gravaSaida.printf("Disponivel%n");
        gravaSaida.printf("%d%n%d%n%d%n%d%n", carretaDisp, carroDisp, motoDisp, vanDisp);
        gravaSaida.printf("Indisponivel%n");
        gravaSaida.printf("%d%n%d%n%d%n%d%n", carretaIndisp, carroIndisp, motoIndisp, vanIndisp);
        
        output.close();
        gravaSaida.close();
    }
    public void SetValorInicial() throws IOException{
        FileWriter output = new FileWriter("src/txt/Veiculo.txt");
        PrintWriter gravaSaida = new PrintWriter(output);
        
        gravaSaida.printf("Disponivel%n");
        gravaSaida.printf("0%n0%n0%n0%n");
        gravaSaida.printf("Indisponivel%n");
        gravaSaida.printf("0%n0%n0%n0%n");
        
        output.close();
        gravaSaida.close();
        
        try {
            GetArquivoVeiculos();
        } catch (IOException ex) {
            Logger.getLogger(Main.class.getName()).log(Level.SEVERE, null, ex);
        }
    }
     public void GetArquivoMargemDeLucro() throws IOException{
        String linha = new String();
        FileInputStream input = new FileInputStream("src/txt/MargemLucro.txt");
        InputStreamReader entradaFormatada = new InputStreamReader(input);
        BufferedReader entradaString = new BufferedReader(entradaFormatada);
        
        while(linha != null){
            linha = entradaString.readLine();
            if("Margem".equals(linha)){
                linha = entradaString.readLine();
                margemLucro = Double.parseDouble(linha);
            }
        } 
        input.close();
    }
    public void SetArquivoMargemDeLucro() throws IOException{
        FileWriter output = new FileWriter("src/txt/MargemLucro.txt");
        PrintWriter gravaSaida = new PrintWriter(output);
        
        gravaSaida.printf("Margem%n");
        gravaSaida.print(margemLucro);
        
        output.close();
        gravaSaida.close();
    }
        public void GetArquivoInfoViagens() throws IOException{
     
        FileInputStream input = new FileInputStream("src/txt/InfoViagens.txt");
        InputStreamReader entradaFormatada = new InputStreamReader(input);
        BufferedReader entradaString = new BufferedReader(entradaFormatada);
     
        String linha = new String();
     
        while(linha != null){
            linha = entradaString.readLine();
                if("MenorCusto".equals(linha)){
                    linha = entradaString.readLine();
                    menorCustoNome = linha;
                    linha = entradaString.readLine();
                    menorCustoTempo = Double.parseDouble(linha);
                    linha = entradaString.readLine();
                    menorCustoCusto = Double.parseDouble(linha);
                }
                if("MaisRapido".equals(linha)){
                    linha = entradaString.readLine();
                    maisRapidoNome = linha;
                    linha = entradaString.readLine();
                    maisRapidoTempo = Double.parseDouble(linha);
                    linha = entradaString.readLine();
                    maisRapidoCusto = Double.parseDouble(linha);
                }
                if("MelhorCxB".equals(linha)){
                    linha = entradaString.readLine();
                    melhorCxBNome = linha;
                    linha = entradaString.readLine();
                    melhorCxBTempo = Double.parseDouble(linha);
                    linha = entradaString.readLine();
                    melhorCxBCusto = Double.parseDouble(linha);
                }
                
        }
        
        input.close();
    }
    public void SetArquivoInfoViagens() throws IOException{
        FileWriter output = new FileWriter("src/txt/InfoViagens.txt");
        PrintWriter gravaSaida = new PrintWriter(output);
        
        gravaSaida.printf("MenorCusto%n");
        gravaSaida.printf("%s%n", menorCustoNome);
        gravaSaida.print(menorCustoTempo);
        gravaSaida.printf("%n");
        gravaSaida.print(menorCustoCusto);
        gravaSaida.printf("%n");
        gravaSaida.print(menorCustoLucro);
        gravaSaida.printf("%n");
        gravaSaida.printf("MaisRapido%n");
        gravaSaida.printf("%s%n", maisRapidoNome);
        gravaSaida.print(maisRapidoTempo);
        gravaSaida.printf("%n");
        gravaSaida.print(maisRapidoCusto);
        gravaSaida.printf("%n");
        gravaSaida.print(maisRapidoLucro);
        gravaSaida.printf("%n");
        gravaSaida.printf("MelhorCxB%n");
        gravaSaida.printf("%s%n", melhorCxBNome);
        gravaSaida.print(melhorCxBTempo);
        gravaSaida.printf("%n");
        gravaSaida.print(melhorCxBCusto);
        gravaSaida.printf("%n");
        gravaSaida.print(melhorCxBLucro);
      
        output.close();
        gravaSaida.close();
    }    
   
       
}