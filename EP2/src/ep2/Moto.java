package ep2;

public class Moto extends VeiculoFlex {
    public Moto(){
        setTipoCombustivel1("Gasolina");
        setTipoCombustivel2("Álcool");
        setPrecoCombustivel1(4.449);
        setPrecoCombustivel2(3.449);
        setRendimentoInicial1(50);
        setRendimentoInicial2(43);
        setPerdaRendimento1(0.3);
        setPerdaRendimento2(0.4);
        setCargaMax(50);
        setVelMedia(110);  
        setCustoViagem(1000000000);
        setTempoViagem(1000000000);
        setCustoBeneficio(1000000000);
    }
    
    /* @Override
    public void Status(){
        System.out.println("Tipo de Veículo: Moto (Flex)" );
        System.out.println("Tipo do 1º Combustível: " + getTipoCombustivel1());
        System.out.println("Preço do 1º Combustível: " + getPrecoCombustivel1());
        System.out.println("Rendimento do 1º Combustível: " + getRendimentoInicial1());
        System.out.println("Perda de Rendimento do 2º Combustível: " + getPerdaRendimento2());
        System.out.println("Tipo do 2º Combustível: " + getTipoCombustivel2());
        System.out.println("Preço do 2º Combustível: " + getPrecoCombustivel2());
        System.out.println("Rendimento do 2º Combustível: " + getRendimentoInicial2());
        System.out.println("Perda de Rendimento do 2º Combustível: " + getPerdaRendimento2());
        System.out.println("Carga Máxima: " + getCargaMax());
        System.out.println("velMedia: " + getVelMedia());
    } */
}
