package ep2;

public class Carro extends VeiculoFlex {
    public Carro(){
        setTipoCombustivel1("Gasolina");
        setTipoCombustivel2("Álcool");
        setPrecoCombustivel1(4.449);
        setPrecoCombustivel2(3.449);
        setRendimentoInicial1(14);
        setRendimentoInicial2(12);
        setPerdaRendimento1(0.025);
        setPerdaRendimento2(0.0231);
        setCargaMax(360);
        setVelMedia(100);
        setCustoViagem(10000000);
        setTempoViagem(10000000);
        setCustoBeneficio(1000000000);
    }
    
    /*@Override
    public void Status(){
        System.out.println("Tipo de Veículo: Carro (Flex)" );
        System.out.println("Tipo do 1º Combustível: " + getTipoCombustivel1());
        System.out.println("Preço do 1º Combustível: " + getPrecoCombustivel1());
        System.out.println("Rendimento do 1º Combustível: " + getRendimentoInicial1());
        System.out.println("Perda de Rendimento do 1º Combustível: " + getPerdaRendimento1());
        System.out.println("Tipo do 2º Combustível: " + getTipoCombustivel2());
        System.out.println("Preço do 2º Combustível: " + getPrecoCombustivel2());
        System.out.println("Rendimento do 2º Combustível: " + getRendimentoInicial2());
        System.out.println("Perda de Rendimento do 2º Combustível: " + getPerdaRendimento2());
        System.out.println("Carga Máxima: " + getCargaMax());
        System.out.println("velMedia: " + getVelMedia());
    }*/  
}
