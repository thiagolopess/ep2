package ep2;

public class Veiculo {
    
    private int cargaMax;
    private int velMedia;
    private double rendimentoViagem;
    private double custoViagem;
    private double tempoViagem;
    private double custoBeneficio;
    
    public int getCargaMax() {
        return cargaMax;
    }

    public void setCargaMax(int cargaMax) {
        this.cargaMax = cargaMax;
    }

    public int getVelMedia() {
        return velMedia;
    }

    public void setVelMedia(int velMedia) {
        this.velMedia = velMedia;
    }
    
    public double getRendimentoViagem() {
        return rendimentoViagem;
    }

    public void setRendimentoViagem(double rendimentoViagem) {
        this.rendimentoViagem = rendimentoViagem;
    }

    public double getCustoViagem() {
        return custoViagem;
    }

    public void setCustoViagem(double custoViagem) {
        this.custoViagem = custoViagem;
    }
     
    public double getTempoViagem() {
        return tempoViagem;
    }

    public void setTempoViagem(double tempoViagem) {
        this.tempoViagem = tempoViagem;
    }

    public double getCustoBeneficio() {
        return custoBeneficio;
    }

    public void setCustoBeneficio(double custoBeneficio) {
        this.custoBeneficio = custoBeneficio;
    }
       
    public void Status() {
       
    }
    
    public boolean VerificaPeso(double carga){
        if(carga > getCargaMax()){
            return false;
        }
        return true;
    }
    public boolean VerificaTempo(double tempoLimite, double distanciaViagem){
        if(distanciaViagem / getVelMedia() > tempoLimite){
            return false;
        }
        return true;
    }
    public void CalculaCusto(double pesoCarga, double distancia){
        
    }
    public void CalculaTempo(double distancia){
        
    }
    public void CalculaCxB(){
        setCustoBeneficio(getCustoViagem()/(getTempoViagem()+1));
    }
}
