package ep2;
        
public class Carreta extends VeiculoNormal {
    public Carreta(){
        setTipoCombustivel("Diesel");
        setPrecoCombustivel(3.869);
        setRendimentoInicial(8);
        setPerdaRendimento(0.0002);
        setCargaMax(30000);
        setVelMedia(60);
        setCustoViagem(1000000000);
        setTempoViagem(1000000000);
        setCustoBeneficio(1000000000);
    }
    
   /* @Override
    public void Status(){
        System.out.println("Tipo de Veículo: Carreta (Normal)");
        System.out.println("Tipo do Combustível: " + getTipoCombustivel());
        System.out.println("Rendimento Inicial: " + getRendimentoInicial());
        System.out.printf("Perda de Rendimento: %.4f\n", getPerdaRendimento());
        System.out.println("Carga Máxima: " + getCargaMax());
        System.out.println("velMedia: " + getVelMedia());
    } */
}
