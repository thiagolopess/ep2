package ep2;

public class Van extends VeiculoNormal {
    public Van(){
        setTipoCombustivel("Diesel");
        setPrecoCombustivel(3.869);
        setRendimentoInicial(10);
        setPerdaRendimento(0.001);
        setCargaMax(3500);
        setVelMedia(80);
        setCustoViagem(1000000000);
        setTempoViagem(1000000000);
        setCustoBeneficio(1000000000);
    }
    
    /*@Override
    public void Status(){
        System.out.println("Tipo de Veículo: Van (Normal)");
        System.out.println("Tipo do Combustível: " + getTipoCombustivel());
        System.out.println("Rendimento Inicial: " + getRendimentoInicial());
        System.out.println("Perda de Rendimento: " + getPerdaRendimento());
        System.out.println("Carga Máxima: " + getCargaMax());
        System.out.println("velMedia: " + getVelMedia());
    }*/
}
