package ep2;

public class VeiculoFlex extends Veiculo{
    private String tipoCombustivel1;
    private String tipoCombustivel2;
    private double precoCombustivel1;
    private double precoCombustivel2;
    private double rendimentoInicial1;
    private double rendimentoInicial2;
    private double perdaRendimento1;
    private double perdaRendimento2;
    private double rendimentoFinal1;
    private double rendimentoFinal2;
    //1 = Gasolina 2 = Alcool
    
    public String getTipoCombustivel1() {
        return tipoCombustivel1;
    }

    public void setTipoCombustivel1(String tipoCombustivel1) {
        this.tipoCombustivel1 = tipoCombustivel1;
    }

    public String getTipoCombustivel2() {
        return tipoCombustivel2;
    }

    public void setTipoCombustivel2(String tipoCombustivel2) {
        this.tipoCombustivel2 = tipoCombustivel2;
    }

    public double getPrecoCombustivel1() {
        return precoCombustivel1;
    }

    public void setPrecoCombustivel1(double precoCombustivel1) {
        this.precoCombustivel1 = precoCombustivel1;
    }

    public double getPrecoCombustivel2() {
        return precoCombustivel2;
    }

    public void setPrecoCombustivel2(double precoCombustivel2) {
        this.precoCombustivel2 = precoCombustivel2;
    }

    public double getRendimentoInicial1() {
        return rendimentoInicial1;
    }

    public void setRendimentoInicial1(double rendimentoInicial1) {
        this.rendimentoInicial1 = rendimentoInicial1;
    }

    public double getRendimentoInicial2() {
        return rendimentoInicial2;
    }

    public void setRendimentoInicial2(double rendimentoInicial2) {
        this.rendimentoInicial2 = rendimentoInicial2;
    }

    public double getPerdaRendimento1() {
        return perdaRendimento1;
    }

    public void setPerdaRendimento1(double perdaRendimento1) {
        this.perdaRendimento1 = perdaRendimento1;
    }

    public double getPerdaRendimento2() {
        return perdaRendimento2;
    }

    public void setPerdaRendimento2(double perdaRendimento2) {
        this.perdaRendimento2 = perdaRendimento2;
    }

    public double getRendimentoFinal1() {
        return rendimentoFinal1;
    }

    public void setRendimentoFinal1(double rendimentoFinal1) {
        this.rendimentoFinal1 = rendimentoFinal1;
    }

    public double getRendimentoFinal2() {
        return rendimentoFinal2;
    }

    public void setRendimentoFinal2(double rendimentoFinal2) {
        this.rendimentoFinal2 = rendimentoFinal2;
    }
    
    
    public void CalculaRendimentoFinal1(double peso){
        setRendimentoFinal1(getRendimentoInicial1() - (peso * getPerdaRendimento1()));
    }
    public void CalculaRendimentoFinal2(double peso){
        
        setRendimentoFinal2(getRendimentoInicial2() - (peso * getPerdaRendimento2()));
    }
    
    @Override
    public void CalculaCusto(double pesoCarga, double distancia){
        Arquivo arquivo = new Arquivo();
        
        
        double custoViagem1, custoViagem2;
        custoViagem1 = (distancia / getRendimentoFinal1()) * getPrecoCombustivel1();
        custoViagem2 = (distancia / getRendimentoFinal2()) * getPrecoCombustivel2();
        if(custoViagem1 < custoViagem2){
            setCustoViagem(custoViagem1);
        }else{
            setCustoViagem(custoViagem2);
        }
        setRendimentoViagem(rendimentoFinal1);
    }
    @Override
    public void CalculaTempo(double distancia){
        double tempo;
        tempo = distancia / getVelMedia();
        
        setTempoViagem(tempo);
    }
}
