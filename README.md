# EP2 - OO 2019.1 (UnB - Gama)

## Sobre  

Projeto feito na IDE **netbeans 8.02**, utilizando **Java 8** no **Windows**.
Os arquivos necessários para rodar o programa se encontram no diretório **EP2/src**.

No diretório **EP2**, se encontra o **diagrama de classes** feito a partir de um plugin do netbeans, chamado **easyUML**. O arquivo foi salvo em png.

A interface possui todas as informações necessárias para uso. 

obs.: Não houve trabalho infantil. **Juro**.
